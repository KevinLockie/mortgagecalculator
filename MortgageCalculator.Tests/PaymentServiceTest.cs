﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MortgageCalculator.Models;
using System.ComponentModel.DataAnnotations;

namespace MortgageCalculator.Tests
{
    [TestClass]
    public class PaymentServiceTest
    {
        public PaymentService PaymentService { get; }
        public InterestRateService InterestRateService { get; }

        public PaymentServiceTest()
        {
            InterestRateService = new InterestRateService();
            PaymentService = new PaymentService(InterestRateService);
        }

        [TestMethod]
        public void MinimumDownPayment()
        {
            Assert.AreEqual(PaymentService.MinimumDownPayment(100000), 5000);
            Assert.AreEqual(PaymentService.MinimumDownPayment(200000), 10000);
            Assert.AreEqual(PaymentService.MinimumDownPayment(500000), 25000);
            Assert.AreEqual(PaymentService.MinimumDownPayment(750000), 50000);
            Assert.AreEqual(PaymentService.MinimumDownPayment(1000000), 200000);
        }

        [TestMethod]
        public void ValidateDownPayments()
        {
            Assert.IsTrue(PaymentService.ValidateDownPayment(100000, 5000));
            Assert.IsTrue(PaymentService.ValidateDownPayment(500000, 25000));
            Assert.IsTrue(PaymentService.ValidateDownPayment(750000, 50000));
            Assert.IsTrue(PaymentService.ValidateDownPayment(1000000, 200000));

            Assert.IsFalse(PaymentService.ValidateDownPayment(100000, 4999.99));
            Assert.IsFalse(PaymentService.ValidateDownPayment(200000, 9999.99));
            Assert.IsFalse(PaymentService.ValidateDownPayment(1000000, 74999.99));
            Assert.IsFalse(PaymentService.ValidateDownPayment(1000000, 199999.99));
        }

        [TestMethod]
        public void MortgageInsurance()
        {
            Assert.AreEqual(PaymentService.MortgageInsurance(100000, 5000), 3150);
            Assert.AreEqual(PaymentService.MortgageInsurance(100000, 10000), 2400);
            Assert.AreEqual(PaymentService.MortgageInsurance(100000, 20000), 0);

            Assert.AreEqual(PaymentService.MortgageInsurance(1000000, 5000), 0);
            Assert.AreEqual(PaymentService.MortgageInsurance(1000000, 15000), 0);
        }

        [TestMethod]
        public void GetPayments()
        {
            GetPayment payment = new GetPayment
            {
                AmortizationPeriod = 5,
                AskingPrice = 100000,
                DownPayment = 20000,
                PaymentSchedule = PaymentFrequency.Monthly
            };
            Assert.AreEqual(PaymentService.GetPayment(payment), 1419.79);

            payment.AmortizationPeriod = 25;
            payment.AskingPrice = 750000;
            payment.DownPayment = 50000;
            payment.PaymentSchedule = PaymentFrequency.Biweekly;
            Assert.AreEqual(PaymentService.GetPayment(payment), 1493.58);

            PatchInterestRate interestRate = new PatchInterestRate
            {
                NewInterestRate = 0.05
            };
            InterestRateService.SetInterestRate(interestRate);

            payment.AmortizationPeriod = 15;
            payment.AskingPrice = 1000000;
            payment.DownPayment = 220000;
            payment.PaymentSchedule = PaymentFrequency.Monthly;

            Assert.AreEqual(PaymentService.GetPayment(payment), 6168.19);
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException))]
        public void GetPaymentsFailure()
        {
            GetPayment payment = new GetPayment
            {
                AskingPrice = 100000,
                DownPayment = 4000,
            };

            PaymentService.GetPayment(payment);
        }

        [TestMethod]
        public void GetMortgage()
        {
            GetMortgage mortgage = new GetMortgage
            {
                AmortizationPeriod = 15,
                DownPayment = 20000,
                PaymentAmount = 533.43135,
                PaymentSchedule = PaymentFrequency.Monthly
            };

            Assert.AreEqual(PaymentService.GetMortgage(mortgage), 100000);

            mortgage.AmortizationPeriod = 25;
            mortgage.DownPayment = 0;
            mortgage.PaymentAmount = 1444.82199;
            mortgage.PaymentSchedule = PaymentFrequency.Biweekly;


            Assert.AreEqual(PaymentService.GetMortgage(mortgage), 700000);
        }
    }
}
