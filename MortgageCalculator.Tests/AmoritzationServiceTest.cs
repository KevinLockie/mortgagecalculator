﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MortgageCalculator.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace MortgageCalculator.Tests
{
    [TestClass]
    public class AmoritzationServiceTest
    {
        public AmortizationService AmoritzationService { get; }

        public AmoritzationServiceTest()
        {
            AmoritzationService = new AmortizationService();
        }
        
        [TestMethod]
        public void ScheduleConversion()
        {
            Assert.AreEqual(AmoritzationService.ScheduleConversion(PaymentFrequency.Weekly), Constants.PeriodWeekly);
            Assert.AreEqual(AmoritzationService.ScheduleConversion(PaymentFrequency.Biweekly), Constants.PeriodBiweekly);
            Assert.AreEqual(AmoritzationService.ScheduleConversion(PaymentFrequency.Monthly), Constants.PeriodMonthly);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ScheduleConversionFailure()
        {
            AmoritzationService.ScheduleConversion((PaymentFrequency)4);
        }

        [TestMethod]
        public void ValidatePeriod()
        {
            Assert.IsFalse(AmoritzationService.ValidatePeriod(0));
            Assert.IsFalse(AmoritzationService.ValidatePeriod(Constants.AmorizationMinimum + Constants.AmorizationMaximum));

            Assert.IsTrue(AmoritzationService.ValidatePeriod(Constants.AmorizationMinimum));
            Assert.IsTrue(AmoritzationService.ValidatePeriod(Constants.AmorizationMaximum));
        }

        [TestMethod]
        public void TotalPayments()
        {
            var amortization = new GetPayment
            {
                PaymentSchedule = PaymentFrequency.Monthly,
                AmortizationPeriod = Constants.AmorizationMinimum
            };

            Assert.AreEqual(AmoritzationService.TotalPayments(amortization), (Constants.AmorizationMinimum * Constants.PeriodMonthly));

            amortization.AmortizationPeriod = Constants.AmorizationMaximum;

            Assert.AreEqual(AmoritzationService.TotalPayments(amortization), (Constants.AmorizationMaximum * Constants.PeriodMonthly));

            amortization.PaymentSchedule = PaymentFrequency.Weekly;

            Assert.AreEqual(AmoritzationService.TotalPayments(amortization), (Constants.AmorizationMaximum * Constants.PeriodWeekly));
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException))]
        public void TotalPaymentsFailure()
        {
            var amortization = new GetPayment
            {
                PaymentSchedule = PaymentFrequency.Monthly,
                AmortizationPeriod = Constants.AmorizationMinimum + Constants.AmorizationMaximum
            };

            AmoritzationService.TotalPayments(amortization);
        }
    }
}
