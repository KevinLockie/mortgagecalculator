﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MortgageCalculator.Models;

namespace MortgageCalculator.Tests
{
    [TestClass]
    public class InterestRateServiceTest
    {
        public InterestRateService InterestRateService { get; }

        public InterestRateServiceTest()
        {
            InterestRateService = new InterestRateService();
        }

        [TestMethod]
        public void SetInterestRate()
        {
            Assert.AreEqual(InterestRateService.InterestRate, Constants.DefaultInterestRate);

            var patch = new PatchInterestRate
            {
                NewInterestRate = 0.05
            };

            patch = InterestRateService.SetInterestRate(patch);

            Assert.AreEqual(patch.OldInterestRate, Constants.DefaultInterestRate);
            Assert.AreEqual(patch.NewInterestRate, 0.05);
            Assert.AreEqual(InterestRateService.InterestRate, 0.05);
        }
    }
}
