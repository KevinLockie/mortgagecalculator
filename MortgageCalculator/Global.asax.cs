﻿using Microsoft.Extensions.DependencyInjection;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MortgageCalculator
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            var services = new ServiceCollection();
            ConfigureServices(services);
            var resolver = new DefaultDependencyResolver(services.BuildServiceProvider());
            DependencyResolver.SetResolver(resolver);

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient(typeof(InterestRateService));
            services.AddTransient(typeof(PaymentService));
        }
    }
}
