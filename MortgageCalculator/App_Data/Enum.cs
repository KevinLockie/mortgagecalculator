﻿namespace MortgageCalculator
{
    public enum PaymentFrequency
    {
        Monthly,
        Biweekly,
        Weekly
    }
}