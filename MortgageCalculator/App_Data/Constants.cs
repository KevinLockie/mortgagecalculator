﻿using System.Collections.Generic;

namespace MortgageCalculator
{
    public class Constants
    {
        public static readonly double DefaultInterestRate = 0.025;

        public static readonly int AmorizationMinimum = 5;
        public static readonly int AmorizationMaximum = 25;

        public static readonly double PeriodWeekly = 365/7D;
        public static readonly double PeriodBiweekly = 365/14D;
        public static readonly double PeriodMonthly = 12D;

        public static readonly double DownPaymentLimit = 500000;
        public static readonly double DownPaymentRate = 0.05;
        public static readonly double DownPaymentOver = 0.1;
        public static readonly double DownPaymentCutoff = 0.2;

        public static readonly double InsuranceCutoff = 1000000;
        public static readonly SortedDictionary<double, double> InsuranceRates = new SortedDictionary<double, double>
        {
            { 0.05, 0.0315},
            { 0.10, 0.024},
            { 0.15, 0.018},
            { 0.20, 0},
        };
    }
}