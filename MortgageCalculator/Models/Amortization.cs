﻿namespace MortgageCalculator.Models
{
    public abstract class Amortization
    {
        public PaymentFrequency PaymentSchedule { get; set; }
        public int AmortizationPeriod { get; set; }
    }
}