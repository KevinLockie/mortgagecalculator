﻿namespace MortgageCalculator.Models
{
    public class GetPayment : Amortization
    {
        public double AskingPrice { get; set; }
        public double DownPayment { get; set; }
    }
}