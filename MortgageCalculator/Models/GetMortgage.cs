﻿namespace MortgageCalculator.Models
{
    public class GetMortgage : Amortization
    {
        public double PaymentAmount { get; set; }
        public double DownPayment { get; set; }
    }
}