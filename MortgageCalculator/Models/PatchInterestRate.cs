﻿namespace MortgageCalculator.Models
{
    public class PatchInterestRate
    {
        public double NewInterestRate { get; set; }
        public double OldInterestRate { get; set; }
    }
}