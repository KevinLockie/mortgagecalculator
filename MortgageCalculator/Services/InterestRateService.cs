﻿using MortgageCalculator.Models;

namespace MortgageCalculator
{
    public class InterestRateService
    {
        /// <summary>
        /// Stores the interest rate and sets the default to the constant.
        /// </summary>
        public double InterestRate { get; private set; } = Constants.DefaultInterestRate;

        /// <summary>
        /// Updates the stored interest rate and returns the old and new interest rate.
        /// </summary>
        /// <param name="interestRate">Converted JSON object from the client.</param>
        /// <returns>Updated JSON object with the old interest rate set.</returns>
        public PatchInterestRate SetInterestRate(PatchInterestRate interestRate)
        {
            interestRate.OldInterestRate = InterestRate;
            InterestRate = interestRate.NewInterestRate;
            return interestRate;
        }
    }
}