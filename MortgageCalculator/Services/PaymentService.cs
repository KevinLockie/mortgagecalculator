﻿using MortgageCalculator.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MortgageCalculator
{
    public class PaymentService : AmortizationService
    {
        public PaymentService() : this(new InterestRateService())
        {
        }

        public PaymentService(InterestRateService interestRate)
        {
            InterestRate = interestRate;
        }

        public InterestRateService InterestRate { get; }

        /// <summary>
        /// Validates that the down payment is equal or larger than the minimum down payment.
        /// </summary>
        /// <param name="AskingPrice">The asking price</param>
        /// <param name="DownPayment">The down payment</param>
        /// <returns>True if the down payment is greater or equal to the minimum.</returns>
        public bool ValidateDownPayment(double AskingPrice, double DownPayment)
        {
            return MinimumDownPayment(AskingPrice) <= DownPayment;
        }

        /// <summary>
        /// Calculates the minimum down payment. If the asking price is larger than the mortgage insurance cutoff, a fixed percentage must be used.
        /// The minimum down payment increases if the asking price is greater than a fixed limit but below the mortgage insurance cutoff.
        /// </summary>
        /// <param name="AskingPrice">The asking price</param>
        /// <returns>The minimum down payment.</returns>
        public double MinimumDownPayment(double AskingPrice)
        {
            if (AskingPrice >= Constants.InsuranceCutoff)
            {
                return AskingPrice * Constants.DownPaymentCutoff;
            }

            return AskingPrice <= Constants.DownPaymentLimit ?
                AskingPrice * Constants.DownPaymentRate :
                Constants.DownPaymentLimit * Constants.DownPaymentRate + (AskingPrice - Constants.DownPaymentLimit) * Constants.DownPaymentOver;
        }

        /// <summary>
        /// Calculates the mortgage insurance based on the asking price and down payment. Above the insurance cuttoff, no mortgage insurance is allowed, otherwise
        /// it will be calculated based on the size of the down payment. The greater the size of the down payment relative to the asking price, the lower the 
        /// mortgage insurance will be.
        /// </summary>
        /// <param name="AskingPrice">The asking price</param>
        /// <param name="DownPayment">The down payment</param>
        /// <returns>The calculated mortgage insurance</returns>
        public double MortgageInsurance(double AskingPrice, double DownPayment)
        {
            if (AskingPrice >= Constants.InsuranceCutoff)
            {
                return 0;
            }

            var downPaymentPercentage = DownPayment / AskingPrice;
            var insurance = 0.0;

            // Insurance rates are stored in a sorted dictionary for easy extension or modification, rather than a series of constants.
            foreach (KeyValuePair<double, double> rate in Constants.InsuranceRates)
            {
                if (rate.Key > downPaymentPercentage)
                {
                    break;
                }

                insurance = rate.Value * AskingPrice;
            }

            return insurance;
        }

        /// <summary>
        /// Validates the payment and calculates the value of each payment for the GET request.
        /// </summary>
        /// <param name="payment">JSON GET payment value</param>
        /// <returns>The payment value for each period or throws an exception if the validation fails.</returns>
        public double GetPayment(GetPayment payment)
        {
            ValidateGetPayment(payment);

            if (!ValidateDownPayment(payment.AskingPrice, payment.DownPayment))
            {
                throw new ValidationException($"Down Payment is below the minimum payment of {MinimumDownPayment(payment.AskingPrice):C}");
            }

            var loan = payment.AskingPrice + MortgageInsurance(payment.AskingPrice, payment.DownPayment) - payment.DownPayment;
            
            return Math.Round(loan * AmortizationFactor(payment), 2);
        }

        /// <summary>
        /// Calculates the amortization factor for payments or mortgages.
        /// </summary>
        /// <param name="amortization">A payment or mortgage</param>
        /// <returns>The amortization factor</returns>
        private double AmortizationFactor(Amortization amortization)
        {
            var payments = TotalPayments(amortization);
            var rate = InterestRate.InterestRate / ScheduleConversion(amortization.PaymentSchedule);

            return (rate * Math.Pow(1 + rate, payments)) / (Math.Pow(1 + rate, payments) - 1);
        }

        /// <summary>
        /// Validates the mortgage and calculates maximum value of a mortgage based on a payment. The down payment is
        /// added to increase the total value of the mortgage. Mortgage insurance is not factored in to the calculation as
        /// the down payment is an optional field in the calculation.
        /// </summary>
        /// <param name="payment">JSON GET payment value</param>
        /// <returns>The payment value for each period or throws an exception if the validation fails.</returns>
        public double GetMortgage(GetMortgage mortgage)
        {
            ValidateGetMortgage(mortgage);

            return Math.Round(mortgage.PaymentAmount / AmortizationFactor(mortgage) + mortgage.DownPayment, 2);
        }

        /// <summary>
        /// Validates that the payment amount and the amortization fields for a mortgage are non-zero. 
        /// Throws exceptions to send specific messages to the user rather than returning true or false.
        /// 
        /// Down payment is optional.
        /// </summary>
        /// <param name="mortgage">JSON GET mortgage</param>
        public void ValidateGetMortgage(GetMortgage mortgage)
        {
            if (mortgage.PaymentAmount == 0)
            {
                throw new ValidationException("Payment Amount is required");
            }

            ValidateAmortization(mortgage);
        }

        /// <summary>
        /// Validates that the asking price, down payment, and the amortization fields for a payment are non-zero.
        /// Throws exceptions to send specific messages to the user rather than returning true or false.
        /// </summary>
        /// <param name="mortgage">JSON GET payment</param>
        public void ValidateGetPayment(GetPayment payment)
        {
            if (payment.AskingPrice == 0)
            {
                throw new ValidationException("Asking Price is required");
            }
            
            if (payment.DownPayment == 0)
            {
                throw new ValidationException("Down Payment is required");
            }

            ValidateAmortization(payment);
        }
    }
}