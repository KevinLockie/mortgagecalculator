﻿using MortgageCalculator.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace MortgageCalculator
{
    public class AmortizationService
    {
        /// <summary>
        /// Validates that the amortization period is between the minimum and maximum constants.
        /// </summary>
        /// <param name="period">The amortization period</param>
        /// <return>True if it's in the range, inclusive of the limits.</return>
        public bool ValidatePeriod(int period)
        {
            return period >= Constants.AmorizationMinimum && period <= Constants.AmorizationMaximum;
        }

        /// <summary>
        /// Converts between the enum values and numerical constants. The constants are approximations of
        /// weekly (52) and biweekly (26). The difference between 365/7 and 52 is about 0.3%.
        /// </summary>
        /// <param name="frequency">The payment frequency</param>
        /// <returns>An integer representation of the number of periods in a year.</returns>
        public double ScheduleConversion(PaymentFrequency frequency)
        {
            switch(frequency)
            {
                case PaymentFrequency.Weekly:
                    return Constants.PeriodWeekly;
                case PaymentFrequency.Biweekly:
                    return Constants.PeriodBiweekly;
                case PaymentFrequency.Monthly:
                    return Constants.PeriodMonthly;
                default:
                    throw new InvalidOperationException("The payment frequency must be Weekly, Biweekly, or Monthly.");
            }
        }

        /// <summary>
        /// Calculates the total number of payment periods for the full amortization schedule.
        /// </summary>
        /// <param name="amoritzation">Payment or mortgage as both have amortization periods.</param>
        /// <returns>The total number of payment periods. Throws an error if the amortization period validation fails.</returns>
        public double TotalPayments(Amortization amoritzation)
        {
            if (!ValidatePeriod(amoritzation.AmortizationPeriod))
            {
                throw new ValidationException($"Amortization period must be a minimum of {Constants.AmorizationMinimum} and a maximum of {Constants.AmorizationMaximum} years.");
            }
            return amoritzation.AmortizationPeriod * ScheduleConversion(amoritzation.PaymentSchedule);
        }

        /// <summary>
        /// Validates that the amortization period has been entered.
        /// </summary>
        /// <param name="amortization">A payment or mortgage</param>
        public void ValidateAmortization(Amortization amortization)
        {
            if (amortization.AmortizationPeriod == 0)
            {
                throw new ValidationException("Amortization Period is required");
            }
        }
    }
}