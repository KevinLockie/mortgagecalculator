﻿using MortgageCalculator.Models;
using System;
using System.Net;
using System.Web.Mvc;

namespace MortgageCalculator.Controllers
{
    public class HomeController : Controller
    {
        public PaymentService PaymentService { get; }

        public HomeController() : this(new PaymentService())
        {   
        }

        public HomeController(PaymentService paymentService)
        {
            PaymentService = paymentService;
        }

        public ActionResult Index()
        {
            return View(PaymentService.InterestRate.InterestRate);
        }

        /// <summary>
        /// GET payment action
        /// </summary>
        /// <param name="json">GET request in JSON format</param>
        /// <returns>Calculated payment value on success, error message otherwise.</returns>
        public JsonResult Payment(GetPayment json)
        {
            try
            {
                return Json(PaymentService.GetPayment(json));
            }
            catch(Exception e)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(e.Message);
            }
            
        }

        /// <summary>
        /// GET mortgage action
        /// </summary>
        /// <param name="json">GET request in JSON format</param>
        /// <returns>Calculated mortgage value on success, error message otherwise.</returns>
        public JsonResult Mortage(GetMortgage json)
        {
            try
            {
                return Json(PaymentService.GetMortgage(json));
            }
            catch (Exception e)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(e.Message);
            }
            
        }

        /// <summary>
        /// PATCH interest rate action
        /// </summary>
        /// <param name="json">PATCH request in JSON format</param>
        /// <returns>A message indicating the old and new interest rates.</returns>
        public JsonResult InterestRate(PatchInterestRate json)
        {
            var result = PaymentService.InterestRate.SetInterestRate(json);

            return Json($"Interest rate changed from {result.OldInterestRate}% to {result.NewInterestRate}%");
        }
    }
}